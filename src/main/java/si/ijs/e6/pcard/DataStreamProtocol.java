package si.ijs.e6.pcard;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by matjaz on 7/31/17.
 * 
 * This class implements data streaming protocols for unpacking packets into samples.
 *
 * MD: checked for its use ^
 *
 * Interface for all implemented data stream protocols.
 *  DataStream ds = DataStreamProtocol.createDataStream( streamType );
 *  // pass the raw packets through stream
 *  for ( RawDataStreamPacket p : _received_packets_ ) {
 *      ds.unpack(p);
 *      // check for errors
 *      if (ds.errors()) {...}
 *  }
 *
 */
public class DataStreamProtocol {
    public static final long INVALID_GADGET_TIME = -1;
    public static final long INVALID_SAMPLE_COUNTER = -1;

    private static Logger logger = Logger.getLogger(DataStreamProtocol.class.getName());

    /**
     * Interface for all implemented data stream protocols.
     *  DataStream ds = createDataStream( streamType );
     *  // pass the raw packets through stream
     *  for ( RawDataStreamPacket p : _received_packets_ ) {
     *      ds.unpack(p);
     *      // check for errors
     *      if (ds.errors()) {...}
     *  }
     */
    public interface DataStream {
        /**
         * Unpack the rawData into UnpackedEcgData instance.
         *
         * @param rawDataStreamPacket the input data stream packet to process (it is allowed to make
         *                            references to it)
         * @return an instance of UnpackedEcgData, converted from the input packet
         */
        UnpackedEcgData unpack(final RawDataStreamPacket rawDataStreamPacket);

        /**
         * Notify the data stream a disconnect occurred; this knowledge will be used when unpacking packets.
         */
        void notifyOfConnect();

        /**
         * Query for errors, if the returned string size is 0, then no errors were encountered
         * @param clear true to clear the errors string; false to leave it (new errors will concatenate to existing log string)
         * @return the error log string
         */
        String getErrors(boolean clear);
    }

    /**
     * Factory method for DataStream.
     * @param streamType type of a stream to create
     * @return the appropriate instance of DataStream implementation.
     */
    public static DataStream createDataStream(BleDataStreamType streamType) {
        switch (streamType) {
            case NormalStream:
                return new NormalDataStream();
            case ExtendedStream_0:
                return new ExtendedDataStream();
            default:
                throw new RuntimeException("Unimplemented or undefined stream type cannot be input to DataStream factory");
        }
    }

    /**
     * Class that describes the so called "Normal streaming data" protocol (See PWP)
     * Packets in this protocol contain 14 10-bit ADC samples, a 10-bit sample counter and 2-bit packet id. 
     */
    public static class NormalDataStream implements DataStream {
        /**
         * Helper variable that defines the type of stream on which this unpacker works.
         * In the future, single unpacker could work on different but related stream types, and
         * particular type would be set in its constructor.
         *
         * Normal and extended stream types could work in this way
         */
        public static final BleDataStreamType streamType = BleDataStreamType.NormalStream;
        /**
         * DEBUG flag; enable to output raw bytes and unpacked data of all processed packets to the standard Log.i
         */
        public boolean DEBUG_BT_PACKET_OUTPUT_ENABLED = false;
        /**
         * The last processed (10-bit) packet time, kept for use when the next one is received
         * - for better stream reconstruction
         */
        long lastPacketCounter = INVALID_GADGET_TIME;
        /**
         * Keeping track of the sensor time keeping
         */
        long gadgetCounterHighBits = 0;
        /**
         * Keeps the number of processed packets
         */
        long numProcessedPackets = 0;
        /**
         * Human readable log of errors (concatenating until cleared manually)
         */
        private String errors = "";
        /**
         * A flag indicating a disconnect occurred after the last packet was unpacked (or no samples
         * were unpacked yet)
         */
        boolean discontinuedSampling = true;
        /**
         * Not very accurate extension of the counter from 10-bit to 64-bit precision;
         * will be accurate only if disconnects are marked, pauses are shorter than 7*1024 samples,
         * there is no errors in received counters
         */
        long reconstructedFullPacketCounter = INVALID_GADGET_TIME;
        /**
         * An offset to counter imposed from the outside (value will be trimmed to approximately 10
         * bits)
         */
        long externalCounterOffset = 0;

        /**
         * get the active (read from the last processed packet) counter 
         * @return the 10-bit counter value
         */
        public long getLastPacketCounter() {
            return lastPacketCounter;
        }

        /**
         * get the reconstructed value of the (last received) counter 
         * @return full reconstructed value of the sample counter of the last processed packet
         */
        public long getReconstructedExtendedCounter() {
            return reconstructedFullPacketCounter;
        }
        
        /**
         * Query for total number of packets processed by this stream
         * @return number of packets processed so far
         */
        public long getNumProcessedPackets() {
            return numProcessedPackets;
        }

        /**
         * Unpack the rawData into UnpackedEcgData instance.
         *
         * @param rawDataStreamPacket the input data stream packet to process
         * @return an instance of UnpackedEcgData, converted from the input packet
         */
        @Override
        public UnpackedEcgData unpack(final RawDataStreamPacket rawDataStreamPacket) {
            UnpackedEcgData data = new UnpackedEcgData();
            data.times = new long[1];
            data.times[0] = rawDataStreamPacket.receivedNanoTime;
            // Normal ECG data stream knows no data scaling (it is globally rather than individually set for all packets); so set a constant

            // Normal ECG stream packets should be 19 bytes long
            if (rawDataStreamPacket.getRawData().length == streamType.packetByteSize) {
                // - 140 bits: 14 10-bit ECG samples
                // - 10 bits: timestamp
                // - 2 bits represent the packet type

                if (DEBUG_BT_PACKET_OUTPUT_ENABLED)
                    logger.finest("DEBUG_BT_PACKET: original: " + Arrays.toString(rawDataStreamPacket.getRawData()));

                // initialize the return values
                data.values = new int[streamType.numSamplesPerPacket];
                reconstructedFullPacketCounter = INVALID_GADGET_TIME;

                // allZero boolean detects corrupted packets that contain all bytes equal to 0; 
                // first requirement for detecting a packet as corrupted is that the expected time of the received packet is not zero ;
                // later on, all additional conditioned will be logically and-ed to this all-zero 'state'
                boolean allZero = (((lastPacketCounter + streamType.packetByteSize) & 0x03FF) != 0);

                // source data will be unpacked using MultiBitBuffer 
                MultiBitBuffer mbb = new MultiBitBuffer(rawDataStreamPacket.getRawData());
                int destIndex = 0;
                // expand the samples
                for (int i = 0; i < streamType.numSamplesPerPacket; ++i) {
                    int temp = mbb.getInt(i * streamType.sampleBitWidth, streamType.sampleBitWidth);
                    data.values[destIndex] = temp;
                    ++destIndex;
                    allZero &= (temp == 0);
                }

                // expand the counter (aka time) from the end of bitstream
                data.rawSampleCounter = mbb.getInt(streamType.numSamplesPerPacket * streamType.sampleBitWidth, streamType.perPacketCounterBitWidth);
                // introduce temporary for assessing very slight fixes (max single overflow)
                int mildlyFixedCounter = data.rawSampleCounter;
                allZero &= (data.rawSampleCounter == 0);

                if (!discontinuedSampling && (data.rawSampleCounter == lastPacketCounter) || (allZero)) {
                    // most likely an error - the same packet was sent twice or a corrupted all-zero packet was received
                    logError(allZero ? "Received a corrupted (all-zero) packet." :
                            "Received a packet with the same sampling time as the last packet.");
                } else if (data.rawSampleCounter < lastPacketCounter) {
                    gadgetCounterHighBits++;
//                    data.rawSampleCounter += 1024;
                    mildlyFixedCounter += 1024;

                    if (DEBUG_BT_PACKET_OUTPUT_ENABLED)
                        logger.finest("DEBUG_BT_PACKET: translated: " + data.rawSampleCounter + " : " + Arrays.toString(data.values));
                }

                if (!discontinuedSampling) {
                    // see if sample counter is progressing as designed: calculate the difference between this and previous counter
                    int diff = (int) ((mildlyFixedCounter - lastPacketCounter) & 0x03FF);
                    // the difference should be divisible by 14: if it is not, more than 1024 samples have passed; flag the counter uncertainty
                    if (discontinuedSampling || ((diff % streamType.numSamplesPerPacket) != 0)) {
                        data.sampleCounterUnsure = true;
                        // max 7 tries (possibly one too many)
                        for (int i = 0; i < 7; ++i) {
                            gadgetCounterHighBits++;
                            diff += 1024;
                            if ((diff % 14) == 0)
                                break;
                        }
                        // if at the end the difference is still off, then this is truly a difficult error
                        if ((diff % 14) != 0)
                            logError("Cannot estimate number of overflows from sample counter");
                    }
                }

                // TODO: check - it seems lastPacketCounter seems the same as data.rawSampleCounter
                lastPacketCounter = data.rawSampleCounter & 0x03FF;
                long oldReconstructedFullPacketCounter = reconstructedFullPacketCounter;
                reconstructedFullPacketCounter = ((gadgetCounterHighBits << 10) + lastPacketCounter) + externalCounterOffset;
                data.sampleCounter = reconstructedFullPacketCounter;
                data.discontinuedSampling = discontinuedSampling;
                discontinuedSampling = false;
                numProcessedPackets++;

                if (oldReconstructedFullPacketCounter > reconstructedFullPacketCounter )
                    logger.warning("Error in the algorithm!");

                if (DEBUG_BT_PACKET_OUTPUT_ENABLED)
                    logger.finest("tenbitcounter = "+data.rawSampleCounter+" (reconstructed) = "+ reconstructedFullPacketCounter);
            } else {
                logError("Invalid length of input data (" + rawDataStreamPacket.getRawData().length + ").");
            }
            return data;
        }

        @Override
        public void notifyOfConnect() {
            discontinuedSampling = true;
        }

        /**
         * Reset the internal state of the stream; next processed packet will be considered the first, etc.
         */
        public void reset() {
            numProcessedPackets = 0;
            discontinuedSampling = true;
            gadgetCounterHighBits = 0;
        }

        /**
         * Tell this data stream to offset its sample counter by the given offset.
         * @param offset a value to offset counter by
         */
        public void imposeCounterOffset(long offset) {
            externalCounterOffset += offset;
            if (externalCounterOffset < 0) {
                long f = (externalCounterOffset-1023)/1024;
                externalCounterOffset += f*1024;
                gadgetCounterHighBits -= f;
            }
            if ((externalCounterOffset & 1024) > 0) {
                long f = (externalCounterOffset/1024);
                if (f > 0) {
                    externalCounterOffset -= f * 1024;
                    gadgetCounterHighBits += f;
                }
            }
            reconstructedFullPacketCounter += offset;
        }

        /**
         * When performing external time synchronization, one might pass its results down to the DataStream unpacking protocol.
         * @param numMissing number of missing packet to account for on next unpack
         */
        @Deprecated
        public void accountForMissingPackets(long numMissing) {
            reconstructedFullPacketCounter += (numMissing+1) * 14;
            lastPacketCounter += reconstructedFullPacketCounter & 0b0000001111111111;
            gadgetCounterHighBits = reconstructedFullPacketCounter >> 10;
        }

        /**
         * Log an error; append it to the end of the error log. 
         * @param err the string to log.
         */
        protected void logError(String err) {
            if (errors.length() > 0)
                errors += "\n";
            errors += err;
        }

        @Override
        public String getErrors(boolean clear) {
            String ret = errors;
            if (clear)
                errors = "";
            return ret;
        }
    }

    /**
     * Currently not fully implemented nor tested
     */
    public static class ExtendedDataStream implements DataStream {
        public static final BleDataStreamType streamType = BleDataStreamType.ExtendedStream_0;
        /**
         * The reconstructed sample counter
         */
        long reconstructedFullSampleCounter = 0;
        /**
         * DEBUG flag; enable to output raw bytes and unpacked data of all processed packets to the standard Log.i
         */
        static final boolean DEBUG_BT_PACKET_OUTPUT_ENABLED = false;
        /**
         * Flag for the disconnect or start of sampling, if true then the received sample will start
         * a stream and will not be related to previously received samples.
         */
        boolean discontinuedSampling = true;

        @Override
        public UnpackedEcgData unpack(RawDataStreamPacket rawDataStreamPacket) {
            UnpackedEcgData data = new UnpackedEcgData();
            data.times = new long[] {rawDataStreamPacket.receivedNanoTime};

            // Normal ECG stream packets should be 19 bytes long
            if (rawDataStreamPacket.getRawData().length == BleDataStreamType.ExtendedStream_0.packetByteSize) {
                // - 140 bits: 14 10-bit ECG samples
                // - 10 bits: timestamp
                // - 2 bits represent the packet type

                if (DEBUG_BT_PACKET_OUTPUT_ENABLED)
                    logger.finest("DEBUG_BT_PACKET: original: " + Arrays.toString(rawDataStreamPacket.getRawData()));

                // initialize the return values
                data.values = new int[streamType.numSamplesPerPacket];
                reconstructedFullSampleCounter = INVALID_SAMPLE_COUNTER;

                // source data will be unpacked using MultiBitBuffer
                MultiBitBuffer mbb = new MultiBitBuffer(rawDataStreamPacket.getRawData());
                int destIndex = 0;
                for (int i = 0; i < streamType.numSamplesPerPacket; ++i) {
                    int temp = mbb.getInt(i * BleDataStreamType.ExtendedStream_0.sampleBitWidth, BleDataStreamType.ExtendedStream_0.sampleBitWidth);
                    data.values[destIndex] = temp;
                    ++destIndex;
                }
                // expand the counter (aka time)
                int packetCounter = mbb.getInt(BleDataStreamType.ExtendedStream_0.numSamplesPerPacket * BleDataStreamType.ExtendedStream_0.sampleBitWidth, BleDataStreamType.ExtendedStream_0.perPacketCounterBitWidth);
                reconstructedFullSampleCounter = packetCounter * streamType.numSamplesPerPacket;

                data.sampleCounter = reconstructedFullSampleCounter;
                data.discontinuedSampling = discontinuedSampling;
                discontinuedSampling = false;
            } else {
                logger.warning("Invalid length of input data (" + rawDataStreamPacket.getRawData().length + ").");
            }
            return data;
        }

        @Override
        public void notifyOfConnect() {
            discontinuedSampling = true;
        }

        @Override
        public String getErrors(boolean clear) {
            // TODO: implement error reporting
            return "";
        }
    }

    /**
     * Class that describes the so called "Normal streaming data" protocol (See PWP)
     * Packets in this protocol contain 14 10-bit ADC samples, a 10-bit sample counter and 2-bit packet id.
     */
    public static class NormalAccelerometerDataStream implements DataStream {
        /**
         * Helper variable that defines the type of stream on which this unpacker works.
         * In the future, single unpacker could work on different but related stream types, and
         * particular type would be set in its constructor.
         *
         * Normal and extended stream types could work in this way
         */
        public static final BleDataStreamType streamType = BleDataStreamType.AccelerometerStreamType0;
        /**
         * DEBUG flag; enable to output raw bytes and unpacked data of all processed packets to the standard Log.i
         */
        public boolean DEBUG_BT_PACKET_OUTPUT_ENABLED = false;
        /**
         * The last processed (10-bit) packet time, kept for use when the next one is received
         * - for better stream reconstruction
         */
        long lastPacketCounter = INVALID_GADGET_TIME;
        /**
         * Keeping track of the sensor time keeping
         */
        long gadgetCounterHighBits = 0;
        /**
         * Keeps the number of processed packets
         */
        long numProcessedPackets = 0;
        /**
         * Human readable log of errors (concatenating until cleared manually)
         */
        private String errors = "";
        /**
         * A flag indicating a disconnect occurred after the last packet was unpacked (or no samples
         * were unpacked yet)
         */
        boolean discontinuedSampling = true;
        /**
         * Not very accurate extension of the counter from 10-bit to 64-bit precision;
         * will be accurate only if disconnects are marked, pauses are shorter than 7*1024 samples,
         * there is no errors in received counters
         */
        long reconstructedFullPacketCounter = INVALID_GADGET_TIME;
        /**
         * An offset to counter imposed from the outside (value will be trimmed to approximately 10
         * bits)
         */
        long externalCounterOffset = 0;

        /**
         * get the active (read from the last processed packet) counter
         * @return the 10-bit counter value
         */
        public long getLastPacketCounter() {
            return lastPacketCounter;
        }

        /**
         * get the reconstructed value of the (last received) counter
         * @return full reconstructed value of the sample counter of the last processed packet
         */
        public long getReconstructedExtendedCounter() {
            return reconstructedFullPacketCounter;
        }

        /**
         * Query for total number of packets processed by this stream
         * @return number of packets processed so far
         */
        public long getNumProcessedPackets() {
            return numProcessedPackets;
        }

        /**
         * Unpack the rawData into UnpackedEcgData instance.
         *
         * This is largely copied form NormalEcgPAcket.unpack, perhaps unifying he code would make sense
         *
         * @param rawDataStreamPacket the input data stream packet to process
         * @return an instance of UnpackedEcgData, converted from the input packet
         */
        @Override
        public UnpackedEcgData unpack(final RawDataStreamPacket rawDataStreamPacket) {
            UnpackedEcgData data = new UnpackedEcgData();
            data.times = new long[1];
            data.times[0] = rawDataStreamPacket.receivedNanoTime;

            // Normal packets should be 20 bytes long
            if (rawDataStreamPacket.getRawData().length == streamType.packetByteSize) {
                // - 3*6*8 bits: six 8-bit 3-axis proper acceleration samples
                // - 14 bits: timestamp
                // - 2 bits represent the packet type

                if (DEBUG_BT_PACKET_OUTPUT_ENABLED)
                    logger.finest("DEBUG_BT_PACKET: original: " + Arrays.toString(rawDataStreamPacket.getRawData()));

                // initialize the return values (num axis is fixed to 3
                data.values = new int[streamType.numSamplesPerPacket*streamType.numChannels];
                reconstructedFullPacketCounter = INVALID_GADGET_TIME;

                // source data will be unpacked using MultiBitBuffer
                MultiBitBuffer mbb = new MultiBitBuffer(rawDataStreamPacket.getRawData());
                int destIndex = 0;

                // expand the samples
                int srcBitIndex = 0;
                for (int i = 0; i < streamType.numSamplesPerPacket; ++i) {
                    for (int j = 0; j < streamType.numChannels; ++j) {
                        int temp = mbb.getInt(srcBitIndex, streamType.sampleBitWidth);
                        srcBitIndex += streamType.sampleBitWidth;
                        data.values[destIndex] = temp;
                        ++destIndex;
                    }
                }

                // expand the counter (aka time) from the end of bitstream
                data.rawSampleCounter = mbb.getInt(srcBitIndex, streamType.perPacketCounterBitWidth);
                if (data.rawSampleCounter < lastPacketCounter) {
                    gadgetCounterHighBits++;

                    if (DEBUG_BT_PACKET_OUTPUT_ENABLED)
                        logger.finest("DEBUG_BT_PACKET: translated: " + data.rawSampleCounter + " : " + Arrays.toString(data.values));
                }

                //reset counter on discontinued sampling? This is a temporary solution
                if (discontinuedSampling) {
                    data.sampleCounterUnsure = true;
                    gadgetCounterHighBits = 0;
                }

                lastPacketCounter = data.rawSampleCounter + (gadgetCounterHighBits << streamType.perPacketCounterBitWidth);
                long oldReconstructedFullPacketCounter = reconstructedFullPacketCounter;
                reconstructedFullPacketCounter = ((gadgetCounterHighBits << streamType.perPacketCounterBitWidth) + lastPacketCounter) + externalCounterOffset;
                data.sampleCounter = reconstructedFullPacketCounter;
                data.discontinuedSampling = discontinuedSampling;
                discontinuedSampling = false;
                numProcessedPackets++;

                if (oldReconstructedFullPacketCounter > reconstructedFullPacketCounter ) {
                    // this warning will be flying for the tiem of testing the above time alignment algorithm
                    logger.warning("Error in the algorithm!");
                }
            } else {
                logError("Invalid length of input data (" + rawDataStreamPacket.getRawData().length + ").");
            }
            return data;
        }

        @Override
        public void notifyOfConnect() {
            discontinuedSampling = true;
        }

        /**
         * Reset the internal state of the stream; next processed packet will be considered the first, etc.
         */
        public void reset() {
            numProcessedPackets = 0;
            discontinuedSampling = true;
            gadgetCounterHighBits = 0;
        }

        /**
         * Tell this data stream to offset its sample counter by the given offset.
         * @param offset a value to offset counter by
         */
        public void imposeCounterOffset(long offset) {
            externalCounterOffset += offset;
            if (externalCounterOffset < 0) {
                long f = (externalCounterOffset-1023)/1024;
                externalCounterOffset += f*1024;
                gadgetCounterHighBits -= f;
            }
            if ((externalCounterOffset & 1024) > 0) {
                long f = (externalCounterOffset/1024);
                if (f > 0) {
                    externalCounterOffset -= f * 1024;
                    gadgetCounterHighBits += f;
                }
            }
            reconstructedFullPacketCounter += offset;
        }

        /**
         * When performing external time synchronization, one might pass its results down to the DataStream unpacking protocol.
         * @param numMissing number of missing packet to account for on next unpack
         */
        @Deprecated
        public void accountForMissingPackets(long numMissing) {
            reconstructedFullPacketCounter += (numMissing+1) * 14;
            lastPacketCounter += reconstructedFullPacketCounter & 0b0000001111111111;
            gadgetCounterHighBits = reconstructedFullPacketCounter >> 10;
        }

        /**
         * Log an error; append it to the end of the error log.
         * @param err the string to log.
         */
        protected void logError(String err) {
            if (errors.length() > 0)
                errors += "\n";
            errors += err;
        }

        @Override
        public String getErrors(boolean clear) {
            String ret = errors;
            if (clear)
                errors = "";
            return ret;
        }
    }

    /**
     * If the type is right (this function will not test it), return the containing ANSI string.
     * This function is used to decipher the string-type packets. Will return garbage if the packet
     * is not of string-type.
     *
     * @param rawDataStreamPacket input data (a raw data stream packet)
     * @return packet data as ANSI string 
     */
    public static String getString(final RawDataStreamPacket rawDataStreamPacket) {
        String ret = "";
        try {
            ret = new String(rawDataStreamPacket.getRawData(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.log(Level.WARNING, "UnsupportedEncodingException when reading string from a data stream packet.", e);
        }

        int eofIndex = ret.indexOf(0);
        if (eofIndex >= 0)
            ret = ret.substring(0, eofIndex);
        return ret;
    }
}
