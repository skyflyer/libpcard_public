package si.ijs.e6.pcard;

/**
 * Created by matjaz on 8/23/18.
 */
public class Pcard {
    //region constants
    // these constants appear mostly in measurement files (.s2, under jurisdiction of libS2Pcard)
    // but not exclusively

    /**
     * A name (human readable string) for ECG sample
     */
    public static final String  ECG_SAMPLE_NAME = "ADC sample";
    /**
     * A unit (represented with a human readable string) for ECG samples
     */
    public static final String  ECG_SAMPLE_UNIT = "mV";
    /**
     * A name (human readable string) for ECG sample counter
     */
    public static final String  ECG_SAMPLE_COUNTER_NAME = "sample counter";
    /**
     * A unit (represented with a human readable string) for ECG sample counters
     */
    public static final String  ECG_SAMPLE_COUNTER_UNIT = "";
    /**
     * A name (human readable string) for ECG stream (A set of ECG samples)
     */
    public static final String  ECG_STREAM_NAME = "ECG stream";
    /**
     * A name (human readable string) for the default recording APP
     */
    public static final String  DEFAULT_RECORDING_APP_NAME = "MobECG";
    //endregion

    public enum CommercialType {
        PRODUCT,
        PROTOTYPE,
        EMULATOR,
        UNKNOWN
    }

    /**
     * Enums used to identify hardware of the gadget. Also provisions basic info and functionality.
     *
     * TODO: missing info in devices: what they communicate as 'name' in BLE advertising
     * TODO:    this is currently encoded in libS2Pcard
     */
    public enum Hardware {
        // the pcbMark (second parameter) is used to identify hardware from s2 files
        Pcard1d0    (CommercialType.PROTOTYPE, "PCARD 1.0",        "PCARD", "transparent",  0.006f, -500*0.006f, 'a'),
        Pcard2d0    (CommercialType.PROTOTYPE, "PCARD 2.0",        "PCARD", "blue/red",     0.006f, -500*0.006f, 'b'),
        Pcard3d0    (CommercialType.PROTOTYPE, "PCARD prototype",  "PCARD", "transitional", 0.006f, 0,           'c'), // unknown offset!
        PcardEsp    (CommercialType.EMULATOR,  "PCARD emulator",   "PCARD", "ESP emulator", 0.006f, -512*0.006f, 'c'),
        Savvy_1d0   (CommercialType.PROTOTYPE, "Savvy 1.0",        "Savvy", "prototype",    0.006f, -512*0.006f, 'd'),
        Savvy_1d2   (CommercialType.PRODUCT,   "Savvy 1.23",       "Savvy", "Model Savvy",  0.006f, -512*0.006f, 'e'),
        Savvy_1d3   (CommercialType.PRODUCT,   "Savvy 1.3",        "Savvy", "Model D",      0.006f, -512*0.006f, 'f'),
        Savvy_1d3_1 (Savvy_1d3,   "Savvy1.3"),
        //       ("PCARD 3.0",  0.012f, 512*0.012f, 'f'), // 12mV range for Savvy 3
        Undefined   (CommercialType.UNKNOWN,   "Undefined",        "unknown","undefined",   0,      0,          '\0');

        /*
        Savvy 1.3
-          Kvant 1 računsko = 6,053978 mikro Volt
-          Kvant 1 nazivno = 6 mikro Volt
-          Kvant 2 računsko = 12,05812 mikro Volt
-          Kvant 2 nazivno = 12 mikro Volt
-          Ničla = projektirana je na sredini območja (dajanska bolj natančna vrednost se dobi iz
                realiziranih vezij – izmeriti in povprečiti)
-          Predvideno je aktivno umerjanje vsakega posameznega primerka na obeh območjih (gain in
                offset na nazivne vrednosti)

        Savvy 1.23
-          Kvant računsko = 5,94367 mikro Volt
-          Kvant nazivno = 6 mikro Volt
-          Ničla = projektirano je sredina območja (dajanska bolj natančna vrednost se dobi iz realiziranih vezij – izmeriti in povprečiti)

        Savvy 1
-          Ne poznam te verzije. Če že obstaja, pa se glede analognega vezja ne razlikuje od Savvy 1.23

        PCARD 1, PCARD 2
-          Kvant računsko = 5,94367 mikro Volt
-          Kvant nazivno = 6 mikro Volt
-          Ničla = projektirano je izven sredine območja na 443 do 478 po A/D enotah. Vrednosti na
                realiziranih vezjih se precej razlikujejo – izmeriti, če je potrebno. Kasneje smo
                offset vrednost neoliko zvišali glede na prvotno projektirano in iz spomina vlečem,
                da je bilo to okrog 500 A/D enot.

        Tako lahko vidiš, da je kvant nazivno pri vseh izvedbah 6 mikro Volt (Savvy 1.3 ima še
        območje z 2x tako velikim kvantom). Najbolje je, da softver na izhodu generira nazivno
        vrednost kvanta.
         */

        /**
         * The product (marketing) name
         */
        String commercialName;
        /**
         * Model name of this hardware, e.g. 'D' as in Savvy model D
         */
        String modelName;
        /**
         * The writing on the pcb; not required for emulators.
         */
        String pcbMark;
        /**
         * This variable defines the type of the device (commercial product / prototype, etc)
         */
        CommercialType commercialType;
        /**
         * Constants that define y = k*x + n linear transformation function.
         * This function is used to transform raw values (ADC readings) to milliVolts.
         */
        public LinearTransformationFunction linearTransformationFunction;
        /**
         * Hadrdware id as stored in firmware version string
         */
        final char firmwareHardwareId;
        /**
         * is this enum just an alternative name for another
         */
        boolean alternativeName = false;

        /**
         * Hardware constructor that is used to create enum values.
         * @param comt           commercial type, e.g. COMMERCIAL or PROTOTYPE
         * @param pcbMark        name written on the PCB, e.g. PCARD 1.0
         * @param commercialName commercial product name, e.g. "Savvy"
         * @param modelName      model name, e.g. "D" or a version, e.g. "1.0"
         * @param k              linear function k (multiplier)
         * @param n              linear function n (offset)
         * @param firmwareId     the character used in firmware version to indicate this particular hw
         */
        Hardware(CommercialType comt, String pcbMark, String commercialName, String modelName, float k, float n, char firmwareId) {
            this.commercialType = comt;
            this.pcbMark = pcbMark;
            this.commercialName = commercialName;
            this.modelName = modelName;
            this.linearTransformationFunction = new LinearTransformationFunction(k, n);
            this.firmwareHardwareId = firmwareId;
        }

        /**
         * Create an alternative hardware name (only pcbMark - used to be humanReadableName) will be different
         * @param original
         * @param pcbNameAlternative
         */
        Hardware(Hardware original, String pcbNameAlternative) {
            this.commercialType = original.commercialType;
            this.pcbMark = original.pcbMark;
            this.commercialName = original.commercialName;
            this.modelName = original.modelName;
            this.linearTransformationFunction = new LinearTransformationFunction(original.linearTransformationFunction.getK(), original.linearTransformationFunction.getN());
            this.firmwareHardwareId = original.firmwareHardwareId;
            this.alternativeName = true;
        }

        /**
         * Overriden Object method toString; returns human readable string representing this hw.
         * @return the human readable string
         */
        @Override
        public String toString() {
            switch (commercialType) {
                case PRODUCT:
                    return commercialName + ", " + modelName;
                case PROTOTYPE:
                    return "prototype "+commercialName+" "+modelName+" (PCB="+pcbMark+")";
                case EMULATOR:
                    return "emulator "+commercialName+" "+modelName;
            }
            return "UNKNOWN device: `"+commercialName+"`, `"+modelName+"` (PCB=`"+pcbMark+"`)";
        }

        /**
         * Get the PCB mark as a string, e.g. Savvy 1.23.
         * Use this string when required to store human readable hardware identifier.
         *
         * @return the pcbMark string stored
         */
        public String getPcbString() {
            return pcbMark;
        }

        /**
         * Get the 'commercial' name for this particular hardware, e.g. Savvy or PCARD
         * @return commercial name string
         */
        public String getCommercialName() {
            return commercialName;
        }

        /**
         * Get the 'model' name for this particular hardware, e.g. Savvy1.3 is Savvy model D
         * @return commercial name string
         */
        public String getModelName() {
            return modelName;
        }

        /**
         * Search for hardware using firmware identification.
         * This is used when firmware string is read and hardware has to be deduced from it.
         * This function will skip alternative named hardware identifiers
         * @param id the character used to identify hardware in firmware version string
         * @return the hardware enum instance
         */
        public static Hardware findHardwareByVersionHwId(char id) {
            for (Hardware hw : Hardware.values()) {
                if ((!hw.alternativeName) && (hw.firmwareHardwareId == id)) {
                    return hw;
                }
            }
            return Undefined;
        }

        /**
         * Search for hardware using case insensitive human readable string (or what is written on the pcb).
         * This is used when a pcb human readable string is read and hardware has to be deduced from it.
         * This function will not return alternative hardware identifiers, but will find their originals instead
         * @param name the string description of the hardware
         * @return the hardware enum instance
         */
        public static Hardware findHardwareByString(String name) {
            for (Hardware hw : Hardware.values()) {
                if (hw.pcbMark.equalsIgnoreCase(name)) {
                    // if alternative name was found, search for the correct Hardware using firmwareId
                    if (hw.alternativeName) {
                        return findHardwareByVersionHwId(hw.firmwareHardwareId);
                    }
                    return hw;
                }
            }
            return Undefined;
        }

        /**
         * Check whether this hardware instance equals to or is newer then the compared instance.
         * @param comparedTo the reference instance to compare to
         * @return true if equal to or newer than the reference
         */
        public boolean isEqualOrNewer(Hardware comparedTo) {
            return (firmwareHardwareId >= comparedTo.firmwareHardwareId);
        }

        /**
         * Check if the hardware supports selection of ECG range (analog amplifier selection)
         * Note, ECG range is supported on SAVVY1.3 or newer and FW 1.03 or higher (but the latter is not checked here)
         * @return true if ECG range selection is supported
         */
        public boolean supportsEcgRangeSelection() {
            return isEqualOrNewer(Savvy_1d3);
        }

        /**
         * Check if the hardware supports software controllable LED.
         * @return true if LED is present on the board
         */
        public boolean supportsLedOutput() {
            return isEqualOrNewer(Savvy_1d0);
        }

        /**
         * Check if the hardware supports low-power mode (via a FET that disconnects the power supply)
         * @return true if low-power mode can be achieved
         */
        public boolean supportsLowPowerMode() {
            return isEqualOrNewer(Savvy_1d2);
        }
    }
}
