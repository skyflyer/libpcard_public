package si.ijs.e6.pcard;

/**
 * Created by matjaz on 10/2/18.
 */

/**
 * This class encompasses the "Annotations" that can be put on moments in measurements
 */
public class Annotations {
    static final String[] builtInActivities = {"RESTING", "ACTIVE", "SPORTS"};
    static final String[] builtInAFeeling = {"DIZZINESS", "DISCOMFORT", "PALPITATIONS"};
    /**
     * Activities are annotated using the given number
     */
    static final int activityAnnotationValue = 0;
    /**
     * 'Feelings' (e.g. how does one feel; not to be mistaken with emotions) are annotated
     * using the givenvalue
     */
    static final int feelingAnnotationValue = 1;
    /**
     * User defined annotations are annotated using the given number
     */
    static final int userDefinedAnnotationValue = 2;
}
