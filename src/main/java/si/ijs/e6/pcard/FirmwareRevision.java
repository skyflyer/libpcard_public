package si.ijs.e6.pcard;


/**
 * Created by matjaz on 7/31/17.
 **/

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by matjaz on 7/31/17.
 * Firmware revision is a helper class for parsing firmware revision string, and for checking
 * revision and hardware-dependant capabilities.
 *
 * Firmware revision strings are of the following form:
 *  "Device 1.2.3x githash" where:
 * <ul>
 *     <li>Device is the device name, e.g. Savvy or Mobecg</li>
 *     <li>1.2.3 is the firmware version in form of major.minor.patch</li>
 *     <li>x is the hardware identifier (always one character long)</li>
 *     <li>githash is the hash code of particular git commit</li>
 * </ul>
 **/
public class FirmwareRevision extends ThreePartVersion {
    private static final Logger logger = Logger.getLogger(FirmwareRevision.class.getName());

    private String rawVersion;
    private String gitNote;

    /**
     * The hardware instance is an enum but an be queried for useful info, Initialized to dummy to
     * make it always available.
     */
    private Pcard.Hardware hardware;

    /**
     * Construct FirmwareRevision instance from the raw string.
     *
     * @param revision the revision string
     */
    public FirmwareRevision(String revision) {
        // parse three part software version first
        super(extractThreePartSoftwareVersion(revision));

        super.applyMobECGPatch();

        // init variables before parsing them further
        rawVersion = revision;
        gitNote = "N/A";
        hardware = Pcard.Hardware.Undefined;
        // parse String and setup variables
        if (!revision.equals(""))
            parseExtendedRawString(revision);
    }

    /**
     * Copy constructor.
     * @param other the instance to copy data from
     */
    public FirmwareRevision(FirmwareRevision other) {
        super(other);
        if (other == null) {
            rawVersion = "N/A";
            hardware = Pcard.Hardware.Undefined;
            gitNote = "N/A";
        } else {
            rawVersion = other.rawVersion;
            hardware = other.hardware;
            gitNote = other.gitNote;
        }
    }

    /**
     * Basic converter to String. Returns all the firmware revision blocks in human readable form.
     * Function to parse this string form is {@link #parseHumanReadableString}
     * @return the revision string in human readable form
     */
    @Override
    public String toString() {
        return "fw="+super.toString()+", hw='"+hardware.getPcbString()+"', git hash="+gitNote;
    }

    /**
     * Get the raw string of the version (as initialized with)
     * @return the raw firmware revision string
     */
    public String getRawString() {
        return rawVersion;
    }

    /**
     * Get firmware as a short string (e.g. for writing it to exported file)
     * @return the revision string in short form
     */
    public String getShortString() {
        return hardware.getPcbString()+"-fw"+super.toString();
    }

    /**
     * Get only revision string (not raw, but reassembled), e.g. 1.2.3
     * @return the revision string after parsing
     */
    public String getRevisionString() {
        return super.toString();
    }

    /**
     * Get the raw string of the version (as initialized with)
     * @return raw revision string
     */
    public String getRawRevisionString() {
        return rawVersion;
    }

    /**
     * Get the raw string for the three-part software revision string.
     * @return the software version
     */
    public String getSoftwareVersionString() {
        return super.getRawString();
    }

    /**
     * Get the raw string for the three-part software revision string.
     * @return the software version
     */
    public ThreePartVersion getSoftwareVersion() {
        return (ThreePartVersion)this;
    }

    /**
     * Get the git note part of the version string.
     * @return the git note string
     */
    public String getGitNote() {
        return gitNote;
    }

    /**
     * Obtain the hardware identifier (the character after the major.minor.patch string).
     * Valid hardware identifiers are: HARDWARE_UNKNOWN, HARDWARE_PCARD_PROTOTYPE_1, HARDWARE_PCARD_PROTOTYPE_2, HARDWARE_SAVVY_1d1,
     *  HARDWARE_SAVVY_1d2, HARDWARE_SAVVY_PROTOTYPE_1
     *
     * Deprecated - use getHardware instead and act on the returned object
     *
     * @return a single character hardware identifier; compare it to known constants to determine hardware
     */
    @Deprecated
    public char getHardwareIdentifier() {
        return hardware.firmwareHardwareId;
    }

    /**
     * Obtain the hardware identifier string as was set during the initialization of this instance (should contain hardware id char only)
     *
     * Deprecated - use getHardware instead and act on the returned object
     *
     * @return the hardware identifier as string 
     */
    @Deprecated
    public String getHardwareIdentifierString() {
        return ""+hardware.firmwareHardwareId;
    }

    /**
     * Returns the associated hardware enum; may equal to Undefined if it cannot be deduced.
     * Before the firmware revision is read, this function will return Undefined. After it is read,
     * it may still return Undefined, if the read string cannot be parsed to a valid hardware.
     *
     * @return Hardware enum; always defined, cannot be null
     */
    public Pcard.Hardware getHardware() {
        return hardware;
    }
    
    //region functionalities
    /**
     * Check if the firmware revision supports more protocols (the basic ECG transmission protocol is always supported)
     * TODO: modify to support querying for various protocols (use DataStreamProtocol class)
     * @return true if protocols are supported
     */
    public boolean supportsProtocols() {
        return minVersion(2, 0, 0);
    }

    /**
     * Check if the firmware revision supports selection of ECG range (analog amplifier selection)
     * Note, ECG range is supported on SAVVY1.3 or newer and FW 1.0.3 or higher
     * @return true if ECG range selection is supported
     */
    public boolean supportsEcgRangeSelection() {
        return hardware.isEqualOrNewer(Pcard.Hardware.Savvy_1d3) && minVersion(1,0,3);
    }

    /**
     * Check whether the firmware revision supports reading calibration report.
     * @return true if calibration report may be read
     */
    public boolean supportsCalibrationReport() {
        return minVersion(1, 0, 3);
    }

    /**
     * Check whether the firmware revision supports reading calibration report.
     * @return true if calibration report may be read
     */
    public boolean supportsFactoryCalibration() {
        return hardware.isEqualOrNewer(Pcard.Hardware.Savvy_1d3) && minVersion(1, 0, 3);
    }

    /**
     * Check whether the firmware revision supports reading calibration report.
     * @return true if calibration report may be read
     */
    public boolean supportsAccelerometer() {
        return hardware.isEqualOrNewer(Pcard.Hardware.Savvy_1d2) && minVersion(1, 0, 5);
    }

    /**
     * Query if this firmware revision allows setting the sampling rate before enabling sampling. 
     * @return true if supported
     */
    public boolean supportsSamplingRateSettingWhenIdle() {
        return minVersion(1, 0, 3);
    }
    
    /**
     * Check if the firmware revision supports querying gadget state
     * @return true if gadget state is supported
     */
    public boolean supportsGadgetState() {
        return minVersion(0,9,0);
    }

    /**
     * Check if the firmware revision supports the 4-digit PIN code
     * @return true if PIN code is required for initializing the gadget
     */
    public boolean supports4NumberPinCode() {
        return minVersion(0,9,0);
    }

    /**
     * Check if the firmware revision supports the ADC channel that samples ECG with around 4 times amplification
     * @return true if the hardware supports the ECG_OUT_TIMES_4 ADC channel
     */
    public boolean supports4xEcgAdcChannel() {
        return hardware.isEqualOrNewer(Pcard.Hardware.Savvy_1d0);
    }

    /**
     * Check if the hardware supports software controllable LED.
     * Note: min firmware version is likely set too high here
     * @return true if LED is present on the board
     */
    public boolean supportsLedOutput() {
        return hardware.supportsLedOutput() && minVersion(1, 0, 0);
    }

    /**
     * Check if the hardware supports low-power mode (via a FET that disconnects the power supply)
     * Note: min firmware version is likely set too high here
     * @return true if low-power mode can be achieved
     */
    public boolean supportsLowPowerMode() {
        return hardware.supportsLowPowerMode() && minVersion(1, 0, 0);
    }
    //endregion

    /**
     * Parse the string, which was read from the gadget;
     * major.minor.patch is parsed as well as hardware revision and git commit hash; this is performed in constructor.
     *
     * Expected string type: "1.2.3h gitnote"
     *
     * @param raw raw input string to parse (including the three part software version, hardware id, and git hash)
     */
    private void parseExtendedRawString(String raw) {
        try {
            // input string of the form "1.2.3h gitnote"
            // split into three part string (sw), followed by hardware identifier (hw) and git hash (git)
            String[] rawSplit = raw.split("\\s", 2);
            // result should be: "[Device] [1.2.3h] [gitnote]"

            // hardware identifier is the last character of second part
            hardware = Pcard.Hardware.findHardwareByVersionHwId(rawSplit[0].isEmpty() ? '?' : rawSplit[0].charAt(rawSplit[0].length()-1));

            // git note is optional
            gitNote = rawSplit.length > 1 ? rawSplit[1] : "";
        } catch (Exception e) {
            logger.log(Level.WARNING, "Unexpected exception while parsing extended raw version string", e);
        }
    }

    /**
     * Helper function that extracts the three-part version string part of the firmware string
     * @param raw raw input string (e.g. "Device 1.2.3x githash ignored_part")
     * @return the the second part of the string that comprises only numbers and dots (e.g. "1.2.3")
     */
    static private String extractThreePartSoftwareVersion(String raw) {
        if (raw.isEmpty())
            return raw;
	    String[] rawSplit = raw.split("\\s", 2);
        return rawSplit.length > 0 ? rawSplit[0].replaceAll("[^0-9.]", "") : null;
    }

    /**
     * Parse the human readable string produced by {@link #toString} .
     *
     * Types of strings that should be parsed:
     *  "fw=1.0.5, hw='Savvy 1.23', git hash=gf235ab35"
     *  "1.0.5 hw=e git_f7ff87d"
     *
     * @param humanReadableString human readable string as given by the {@link #toString}
     * @return the firmware instance
     */
    static public FirmwareRevision parseHumanReadableString(String humanReadableString) {
        FirmwareRevision fwRev = new FirmwareRevision("");

        if (!humanReadableString.isEmpty()) {
            String[] stringSplit = humanReadableString.split(",\\s", 5);
            // Properly defined string:
            //     stringSplit = ["fw=1.0.5",   "hw='Undefined'",   "git",          "hash=gf235ab35"]

            if (stringSplit.length == 1) {
                // Old style string:
                // stringSplit = ["1.0.5",      "hw=e",             "git_f7ff87d"                   ]
                stringSplit = humanReadableString.split("\\s", 5);
            }

            String fw = "";
            char hw = ' ';
            String git = "";
            for (String aStringSplit : stringSplit) {
                String[] split = aStringSplit.split("=", 2);
                if (split.length > 1) {
                    // non empty second part of string, must be the properly defined string

                    /*
                    // remove ending comma (if it is there) TODO: removing comma is no longer required, but produce a test before modifying this code
                    if (split[1].charAt(split[1].length()-1) == ',')
                        split[1].substring(0, split[1].length()-1);
                    */
                    switch (split[0]) {
                        case "fw":
                            fw = split[1];
                            break;
                        case "hw":
                            if (split[1].length() == 1) {
                                hw = split[1].charAt(0);
                            } else {
                                if (split[1].charAt(0) == '\'')
                                    hw = Pcard.Hardware.findHardwareByString(split[1].substring(1, split[1].length()-1)).firmwareHardwareId;
                                else
                                    hw = Pcard.Hardware.findHardwareByString(split[1]).firmwareHardwareId;
                            }
                            break;
                        case "hash":
                        case "git hash":
                            git = split[1];
                            break;
                    }
                } else {
                    // a part of old-style string

                    // git hash will be presented by git____ or by appearing after hw has been defined
                    if (split[0].startsWith("git"))
                        git = split[0].substring(4);
                    else if (hw != ' ')
                        git = split[0];

                    // version starts with a digit
                    else if (Character.isDigit(split[0].charAt(0))) {
                        // version might contain hardware revision - single char postfix
                        if (!Character.isDigit(split[0].charAt(split[0].length()-1))) {
                            // firmware version + hardware revision string, e.g.: "1.0.2e"
                            fw = split[0].substring(0, split[0].length()-1);
                            hw = split[0].charAt(split[0].length()-1);
                        } else {
                            // just normal version string
                            fw = split[0];
                        }
                    }
                }
            }

            return new FirmwareRevision(fw+hw+' '+git);
        }

        return fwRev;
    }
}
