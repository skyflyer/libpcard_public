package si.ijs.e6.pcard;

import java.util.Locale;

/**
 * Created by matjaz on 7/28/15.
 *
 * The class for holding raw data stream packet payloads of any type.
 *
 * RawDataStreamPacket usage:
 *  - Prepare "RawDataStreamPacket myPacket" object as a part of EcgDataSource initialization.
 *  - Set it up myPacket = new RawDataStreamPacket(inputBytes, inputTimestamp)
 *  - Unpack the packet through the class {@link DataStreamProtocol} to access the data inside.
 *
 * MD: checked for its use ^
 *
 * Assumptions:
 *  - raw data packets contain 19 bytes of data or less; RuntimeException is thrown if one that does not is encountered
 */
public class RawDataStreamPacket {
    /**
     * A public constant that declares maximum supported BLE packet payload size.
     */
    public static final int DATA_STREAM_MAX_PAYLOAD = 20;
    /**
     * A reserved value for flagging variable received time as invalid.
     */
    private static final long INVALID_NANO_TIME = -1;
    /**
     * The data received in a single BLE packet.
     */
    public byte[] rawData = null;
    /**
     * The time when the packet has been received by user app.
     * This is the timestamp (in nanoseconds) when the receiver app starts to process packet; it is
     * relative to something (perhaps Android start time?) - do not use its absolute value.
     */
    public long receivedNanoTime = INVALID_NANO_TIME;

    /**
     * The constructor that initializes nothing- to be used in time-critical sections.
     * E.g message handlers can use this constructor. Any observer can determine that the data
     * has not been copied in yet.
     */
    public RawDataStreamPacket() {}

    /**
     * Constructor for data stream, which only copies in the input parameters.
     * Processing of the data can be done later on.
     *
     * @param rawData      raw data bytes from the received BT communication packet.
     * @param timeReceived The timestamp taken when the BT packet was passed to the user app.
     */
    public RawDataStreamPacket(final byte[] rawData, final long timeReceived) {
        this.rawData = new byte[rawData.length];
        grabNewData(rawData, timeReceived);
    }
    
    /**
     * Method that accepts data from a newly received packet and is called within the BLE callback. 
     * A fast method, only performs a single short array copy and another variable copy.
     * Data is deep copied into the RawDataStreamPacket and the original may be safely discarded.
     * 
     * @param rawData       the data of the packet
     * @param timeReceived  the time data was received
     */
    public void grabNewData(final byte[] rawData, final long timeReceived) {
        if (rawData.length > DATA_STREAM_MAX_PAYLOAD)
            throw new RuntimeException("RawDataStreamPacket Received a packet of data that is too large ("+rawData.length+" bytes)");
        System.arraycopy(rawData, 0, this.rawData, 0, rawData.length);
		this.receivedNanoTime = timeReceived;
	}

    /**
     * Method that accepts another instance of the same class, from which it copies its raw data and timestamp.
     * A fast method, only performs a single short array copy and another variable copy.
     *
     * @param other RawDataStreamPacket to copy data and timestamp from.
     */
	public void grabNewData(final RawDataStreamPacket other) {
	    // TODO: could make shallow copy of byte array here (could also mean that rawData could be left uninitialized in constructor)
        grabNewData(other.rawData, other.receivedNanoTime);
    }

    /**
     * Obtain the raw data associated with this packet
     * @return the raw data bytes
     */
    public byte[] getRawData() {
        return rawData;
    }

    /**
     * Returns some basic human readable information about this packet.
     */
    @Override
    public String toString() {
        return String.format(Locale.US, "Data: %d bytes, received at : %.3f us", rawData.length, receivedNanoTime*0.001);
    }
}
