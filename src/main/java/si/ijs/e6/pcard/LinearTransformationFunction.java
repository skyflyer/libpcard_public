package si.ijs.e6.pcard;

/**
 * A canonical form of linear transformation function.
 * This is used as transformation function for mapping PCARD raw ECG samples (in ADC units) to milli Volts
 */
public class LinearTransformationFunction {
    private float k, n;
    private String unit;

    /**
     * Construct linear function as y = x*k + n [unit]; unit is here for convenience
     * @param k       linear coefficient of the linear function
     * @param n       intercept value of the linear function
     * @param unit    unit of the value after this linear transformation has been applied
     */
    public LinearTransformationFunction(float k, float n, String unit) {
        this.k = k;
        this.n = n;
        this.unit = unit;
    }

    /**
     * Construct linear function as y = x*k + n; This constructor will leave unit undefined (empty string)
     * @param k       linear coefficient of the linear function
     * @param n       intercept value of the linear function
     */
    public LinearTransformationFunction(float k, float n) {
        this.k = k;
        this.n = n;
        this.unit = "";
    }

    /**
     * Make a copy of other function; use this function to reconfigure existing instance.
     * @param other the function to make a copy from
     */
    public void copyFrom(LinearTransformationFunction other) {
        if (other != null) {
            this.k = other.k;
            this.n = other.n;
            this.unit = other.unit;
        } else {
            this.k = 0;
            this.n = 0;
            this.unit = "null";
        }
    }

    public void setK(float k) {
        this.k = k;
    }

    public void setN(float n) {
        this.n = n;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * Get the linear coefficient
     * @return the linear coefficient (k in y=k*x+n)
     */
    public float getK() {
        return k;
    }

    /**
     * Get the intercept
     * @return the intercept (n in y=k*x+n)
     */
    public float getN() {
        return n;
    }

    /**
     * Get the linear coefficient
     * @return the linear coefficient (k in y=k*x+n)
     */
    public float getAmplitude() {
        return k;
    }

    /**
     * Get the intercept
     * @return the intercept (n in y=k*x+n)
     */
    public float getOffset() {
        return n;
    }

    /**
     * Use the linear function to transform input number x
     * @param x    the input to function y=x*k+n
     * @return the value y from y=x*k+n
     */
    public float transformFloat(float x) {
        return k * x + n;
    }

    /**
     * Use the linear function to transform input number x
     * @param x    the input to function y=x*k+n
     * @return the value y from y=x*k+n
     */
    public float transformInt(int x) {
        return Math.round(k * x + n);
    }

    /**
     * Query for the unit (of y in y=k*x+n) assigned to this linear function result
     * @return the string representing unit of the y (may be empty)
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Output the linear function as a string: "y=k×x+n", where k and n are the actual values
     * @return the string representation of this linear transformation function
     */
    @Override
    public String toString() {
        return "y="+k+"×x"+(n >= 0 ? "+" : "")+n;
    }
}
