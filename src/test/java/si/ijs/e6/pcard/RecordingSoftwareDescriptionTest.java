package si.ijs.e6.pcard;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.fusesource.jansi.Ansi.ansi;

public class RecordingSoftwareDescriptionTest {
    /**
     * List of possible values (extracted by a script from existing s2 files @ MD computer)
     * commandline:
     * fd -a .s2$ -x $PATH_MOBECG/scripts/grepFirmwareVersion -q  {} \;
     */
    private String[] possibleStringsFor_recording_software = {
            "MobECG 1.8.1-69-g616eabfa (master)",
            ""
    };

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testAllKnownStrings() {
        System.out.printf("Firmware revision original vs toString vs parsed:\n");
        Set<String> temp = new LinkedHashSet<String>( Arrays.asList(possibleStringsFor_recording_software) );
        for (String s : temp) {
            parseSingleString(s);
        }
    }

    private void parseSingleString(String s) {
        // instantiate and parse
        RecordingSoftwareDescription sw = RecordingSoftwareDescription.parseString(s);

        // output parsed fields
        String parsed = "name = "+
                        ansi().fgBrightGreen()+sw.getName()+ansi().reset()+" ver = "+
                        ansi().fgBrightGreen()+sw.getVersion().toString()+ansi().reset()+" othr = "+
                        ansi().fgBrightGreen()+sw.getAdditionalDescription()+ansi().reset();
        System.out.printf(ansi().fgBrightBlue()+"%-37s"+
                ansi().fgBrightGreen()+"%-40s"+ansi().reset()+"%-50s"+ansi().reset()+
                "\n", s, sw.toString(), parsed);
    }

}