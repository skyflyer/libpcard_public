package si.ijs.e6.pcard;

import si.ijs.e6.pcard.MultiBitBuffer;

import static org.junit.Assert.*;

/**
 * Created by matjaz on 10/17/18.
 */
public class MultiBitBufferTest {
    byte[] byteBuffer = new byte[2000];
    MultiBitBuffer buffer = new MultiBitBuffer(byteBuffer);

    void initByteBuffer(int starting, int addition) {
        byte val = (byte)starting;
        for (int i = 0; i < byteBuffer.length; ++i) {
            byteBuffer[i] = val;
            val += addition;
        }
    }

    @org.junit.Test
    public void getInt() throws Exception {
        initByteBuffer(1, 3);
        int val4 = buffer.getInt(8, 8);
        assertEquals(4, val4);
    }

    @org.junit.Test
    public void setInts() throws Exception {
        initByteBuffer(0, 0);
        buffer.setInts(0xFF, 0, 8, 1);
        assertEquals((byte)0xFF, byteBuffer[0]);
    }

    @org.junit.Test
    public void setAndGet() throws Exception {
        final int bitLen = 9;
        final int bitMask = (1 << bitLen)-1;
        for (int ofs = 0; (ofs+bitLen) < 8*byteBuffer.length; ofs+=bitLen)
            buffer.setInts(ofs, ofs, bitLen, 1);

        for (int ofs = 0; (ofs+bitLen) < 8*byteBuffer.length; ofs+=bitLen)
            assertEquals(ofs & bitMask, buffer.getInt(ofs, bitLen) & bitMask);
    }

}