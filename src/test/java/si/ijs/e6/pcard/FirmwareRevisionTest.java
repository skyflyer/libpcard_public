package si.ijs.e6.pcard;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import org.fusesource.jansi.Ansi;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.fusesource.jansi.Ansi.ansi;

public class FirmwareRevisionTest {
    /**
     * List of possible values (extracted by a script from existing s2 files @ MD computer)
     * commandline:
     * fd -a .s2$ -x $PATH_MOBECG/scripts/grepFirmwareVersion -q  {} \;
     */
    private String[] possibleStringsFor_recording_hardware = {
            "Savvy 1.0.2134 hw=e git_1b34dca",
            "Savvy 1.0.2134 hw=e git_1b34dca",
            "Savvy 1.0.3 hw=f git_0d3053c",
            "MobECG 1.0.2 hw=a git_1b34dca",
            "Savvy 1.0.2134 hw=e git_1b34dca",
            "Savvy 1.0.2 hw=f git_a1f7302-d",
            "Savvy 1.0.3 hw=f git_0d3053c",
            "Savvy 1.0.3 hw=f git_0d3053c",
            "Savvy 1.0.5 hw=f git_674159f",
            "Savvy 1.0.5 hw=f git_c83c491",
            "Savvy 1.0.3 hw=f git_0d3053c",
            "Savvy 1.0.5 hw=e git_f7ff87d",
            "Savvy 1.0.5 hw=f git_674159f",
            "Savvy 1.0.3 hw=f git_0d3053c",
            "Savvy 1.0.3 hw=f git_0d3053c",
            "Savvy 1.0.5 hw=f git_674159f",
            "MobECG 1.0.2 hw=b git_b4a88ea",
            "MobECG 1.0.2 hw=a git_b4a88ea",
            "Savvy 1.0.3 hw=f git_b484b79",
            "Savvy 1.0.3 hw=f git_0d3053c",
            "Savvy 1.0.4 hw=f git_674159f",
            "MobECG 1.0.2 hw=a git_b4a88ea",
            "MobECG 1.0.2 hw=b git_b4a88ea",
            "Savvy 1.0.2 hw=e git_b4a88ea",
            "Savvy 1.0.4 hw=f git_674159f",
            "SAVVY 1.0.3 hw=f git_b484b79-d",
            "MobECG 1.0.2 hw=a git_b4a88ea",
            "MobECG 1.0.2 hw=a git_b4a88ea",
            "Savvy 1.0.3 hw=f git_b484b79",
            "Savvy 1.0.3 hw=f git_0d3053c",
            "Savvy 1.0.2 hw=e git_1b34dca",
            "Savvy 1.0.3 hw=f git_0d3053c",
            "Savvy 1.0.2e 1b34dca",
            "Savvy 1.0.2e 1b34dca",
            "MobECG 1.0.2b b4a88ea",
            "Savvy 1.0.2e 1b34dca",
            "Savvy 1.0.2e 1b34dca",
            "Savvy 1.0.2e b4a88ea",
            "Savvy 1.0.2e 1b34dca",
            "Savvy 1.0.2e 1b34dca",
            "Savvy 1.0.2e b4a88ea",
            "Savvy 1.0.2e 1b34dca",
            "MobECG 1.0.2a b4a88ea",
            "Savvy 1.0.2e 1b34dca",
            "Savvy 1.0.0e c9e9b7a",
            "Savvy 1.0.2e 1b34dca",
            "MobECG 1.0.2a b4a88ea",
            "Savvy 1.0.2e 1b34dca",
            "Savvy 1.0.2e 1b34dca",
            "MobECG 1.0.2a b4a88ea",
            "Savvy 1.0.2e 1b34dca",
            "MobECG 1.0.2a b4a88ea",
            "Savvy 1.0.2e 1b34dca",
            "Savvy 1.0.2e 1b34dca",
            "MobECG 1.0.2a b4a88ea",
            "MobECG 1.0.2a b4a88ea",
            "Savvy 1.0.2e 1b34dca",
            "MobECG 1.0.2a b4a88ea",
            "Savvy 1.0.2e 1b34dca",
            "Savvy 1.0.2e b4a88ea",
            "Savvy 1.0.2e 1b34dca",
            "MobECG 0.8b 5dd4b58",
            "MobECG 1.0.2a b4a88ea",
            "Savvy 1.0.2e 1b34dca",
            "MobECG 1.0.2a b4a88ea",
            "MobECG 1.0.2a b4a88ea",
            "Savvy 1.0.2f a1f7302-d",
            "Savvy 1.0.2f a1f7302-d",
            ""
    };

    @Test
    public void parseVersionString() {
        // string to parse
        String testRev="1.0.2e g345345";
        // instanciate and parse
        FirmwareRevision fw = new FirmwareRevision(testRev);

        // output parsed fields
        System.out.printf("Firmware revision input is:\n   %s\n", testRev);

        System.out.printf("Firmware revision is then parsed into:\n   SW = %s\n   HW = %s\n   git= %s\n",
                fw.getSoftwareVersionString(), fw.getHardware().toString(), fw.getGitNote());

        ThreePartVersion tpv = fw.getSoftwareVersion();
        System.out.printf("Extracted software version = %d.%d.%d\n",
                tpv.getValueOfMajor(), tpv.getValueOfMinor(), tpv.getValueOfPatch());

        // assertions against hard coded strings and constants
        assertThat(fw.getSoftwareVersionString(), is("1.0.2"));
        assertThat(fw.getHardware().toString(), is(Pcard.Hardware.Savvy_1d2.toString()));
        assertThat(fw.getGitNote(), is("g345345"));
    }

    @Test
    public void parseHumanReadableVersionString() {
        // string to parse
        String testRev="fw=1.0.5, hw='Savvy 1.23', git hash=gf235ab35";
        // instanciate and parse
        FirmwareRevision fw = FirmwareRevision.parseHumanReadableString(testRev);

        // output parsed fields
        System.out.printf("Firmware revision input is:\n   %s\n", testRev);
        System.out.printf("Firmware revision toString:\n   %s\n", fw.toString());

        System.out.printf("Firmware revision is then parsed into:\n   SW = %s\n   HW = %s\n   git= %s\n",
                fw.getSoftwareVersionString(), fw.getHardware().toString(), fw.getGitNote());

        ThreePartVersion tpv = fw.getSoftwareVersion();
        System.out.printf("Extracted software version = %d.%d.%d\n",
                tpv.getValueOfMajor(), tpv.getValueOfMinor(), tpv.getValueOfPatch());

        // assertions against hard coded strings and constants
        assertThat(fw.getSoftwareVersionString(), is("1.0.5"));
        assertThat(fw.getHardware().toString(), is(Pcard.Hardware.Savvy_1d2.toString()));
        assertThat(fw.getGitNote(), is("gf235ab35"));
    }

    @Test
    public void parseListOfHumaReadableVersionStrings() {
        System.out.printf("Firmware revision original vs toString vs parsed:\n");
        Set<String> temp = new LinkedHashSet<String>( Arrays.asList(possibleStringsFor_recording_hardware) );
        for (String s : temp) {
            parseSingleString(s);
            try {
                parseSingleString(s.split(" ", 2)[1]);
            } catch (Exception e) {
                // do nothing
            }
        }
    }

    private void parseSingleString(String s) {
        // instantiate and parse
        FirmwareRevision fw = FirmwareRevision.parseHumanReadableString(s);

        // output parsed fields
        String parsed = String.format("FW= "+
                        ansi().fgBrightGreen()+"%-6s"+ansi().reset()+" HW= "+
                        ansi().fgBrightGreen()+"%-11s"+ansi().reset()+" git= "+
                        ansi().fgBrightGreen()+"%s"+ansi().reset(),
                fw.getSoftwareVersionString(), fw.getHardware().toString(), fw.getGitNote());
        System.out.printf(ansi().fgBrightBlue()+"%-37s"+
                ansi().fgBrightGreen()+"%-50s"+ansi().reset()+"%-60s"+ansi().reset()+
                "\n", s, fw.toString(), parsed);
    }
}